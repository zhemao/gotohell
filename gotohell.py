#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess
import sys
import time

def maketrans(ufrom, uto):
    table = {}

    for i, char in enumerate(ufrom):
        table[ord(char)] = uto[i]

    return table

RIGHTSIDE_UP = u"abcdefghijklmnopqrstuvwxyz0123456789"
UPSIDE_DOWN =  u"ɐqɔpǝɟɓɥıɾʞlɯuodbɹsʇnʌʍxʎz0⇂ᄅƐㄣގ9ㄥ86"

FLIP_TABLE = maketrans(RIGHTSIDE_UP, UPSIDE_DOWN)

def animate(frames, delay):
    for fr in frames:
        sys.stdout.write("\r\033[K" + fr)
        sys.stdout.flush()
        time.sleep(delay)

def flip(unistr):
    return unicode(unistr).translate(FLIP_TABLE)[::-1]

def main(argv):
    if len(argv) < 2:
        print("Usage: fyou process-name")

    pname = argv[1]

    sys.stdout.write(u"        ,(王八蛋。)\n(,°□°,) " + unicode(pname))
    sys.stdout.flush()

    time.sleep(1)

    blackhole = open(os.devnull, "w")

    ret = subprocess.call(["killall", "-9", pname],
                stdout=blackhole, stderr=blackhole)

    if ret == 0:
        sys.stdout.write(
                u"\r\033[A\033[K        ,(去死吧!)\n(╯°□°）╯︵" + flip(pname))
        sys.stdout.flush()
        time.sleep(1)
    else:
        print("\r\033[A\033[K")
        animate([u"(；￣Д￣)",
                 u"(；￣Д￣) . o O( 艾... )",
                 u"(；￣Д￣) . o O( 艾... )",
                 u"(；￣Д￣) . o O( 艾... )",
                 u"(；￣Д￣) . o O( 艾...没用 )"], 0.5)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
